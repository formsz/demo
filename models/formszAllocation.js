var mongoose = require('mongoose');
var FormszallocationSchema = new mongoose.Schema({
		formId :{type:String,default:null},
		updatedTime:{ type: Date, default: Date.now },
		allotateTo:{type:String,default:null},
		statuscode:{type:Boolean,default:false}
});
module.exports = mongoose.model('Formszallocation', FormszallocationSchema);

