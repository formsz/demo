var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer  = require('multer');
//imports
var routes = require('./routes/index');
var login = require('./routes/login');
var todos = require('./routes/todos');
var users = require('./routes/users');
var store = require('./routes/store');
var group = require('./routes/group');
var admins = require('./routes/admin');
var formsz = require('./routes/formsz');
var formszDetails = require('./routes/formszDetails');
var mongoose = require('mongoose');
var log = require('./routes/log')(module);
var app = express();

var config = require('./lib/config');

/*var connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
  process.env.OPENSHIFT_APP_NAME;
  */
  var connection_string = "root"+ ":" +
  "mm1234" + "@" +
 "172.17.0.9" + ':' +
 "27017" + '/' +
  "formsz";
  db = mongoose.connect('mongodb://'+connection_string, function(err) {
    if(err) {
        console.log('connection error', err);
    } else {
        console.log('connection successful');
    }
});

app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});
// view engine setup
app.set('views', path.join(__dirname, '/views/'));
app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/* app.use(multer({ dest: './fileupload'})); */
//app.all('/api/v1/*', [require('./middlewares/validateRequest')]);   
//app.use('/', routes);
app.use('/', login);
app.use('/api/v1/todos', todos);
app.use('/api/v1/users', users);
app.use('/api/v/store', store);
app.use('/api/v1/group', group);
app.use('/api/v1/admins', admins);
app.use('/api/v1/formsz', formsz);
app.use('/api/v1/formszDetails', formszDetails);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
	
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
   res.render('error', {
      message: err.message,
      error: err
    });
  });
}
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

